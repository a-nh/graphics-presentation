#ifndef SCENE_PHONG
#define SCENE_PHONG

#include "Scene.h"
#include "glslprogram.h"
#include "vbotorus.h"
#include "cookbookogl.h"

#include <glm/glm.hpp>
using glm::mat4;

class ScenePhong : public Scene
{
private:
	GLSLProgram prog;

	int width, height;
	VBOTorus * torus;

	float angle;

	mat4 model;
	mat4 view;
	mat4 projection;

	void setMatrices();
	void compileAndLinkShader();

public:
	ScenePhong();

	void InitScene();
	void Update(float t);
	void Render();
	void Resize(int, int);

};

#endif
