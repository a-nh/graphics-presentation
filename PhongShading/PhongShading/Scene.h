#ifndef SCENE_H
#define SCENE_H

class Scene
{
protected:
	bool m_animate;
public:

	Scene() : m_animate(true) {}
	
	virtual void InitScene() = 0;

	virtual void Update(float t) = 0;

	virtual void Render() = 0;

	virtual void Resize(int, int) = 0;

	void Animate(bool value) 
	{
		m_animate = value;
	}

	bool Animating()
	{
		return m_animate;
	}
};

#endif
