#include "cookbookogl.h"
#include "glutils.h"

#include "GLFW\glfw3.h"
#include "SceneTexture.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

Scene * scene;

GLFWwindow * window;



void InitGL()
{
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

	glDebugMessageCallback(GLUtils::debugCallback, NULL);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
	glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_MARKER, 0,
		GL_DEBUG_SEVERITY_NOTIFICATION, -1, "Start debugging");

	scene->InitScene();
}

void mainLoop()
{
	while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE))
	{
		GLUtils::checkForOpenGLError(__FILE__, __LINE__);
		scene->Update(float(glfwGetTime()));
		scene->Render();
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
}

void ResizeGL(int w, int h)
{
	scene->Resize(w, h);
}

int main()
{
	scene = new SceneTexture();

	if (!glfwInit()) exit(EXIT_FAILURE);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

	string title = "Texture Demotration";
	window = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, title.c_str(), NULL, NULL);

	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	} 

	GLUtils::dumpGLInfo();

	InitGL();
	ResizeGL(WIN_WIDTH, WIN_HEIGHT);

	mainLoop();

	glfwTerminate();

	exit(EXIT_SUCCESS);

	return 0;
}